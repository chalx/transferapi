package org.alex.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.alex.api.entity.Transaction;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferStatus {
    private Transaction.Status status;
    private String message;
}
