package org.alex.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Value;
import org.alex.api.entity.Account;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@Value
public class AccountDto {
    private int id;
    private String name;
    private String accountRef;
    private BigDecimal amount;
    private String currency;
    private List<TransactionDto> sentTransactions;
    private List<TransactionDto> receivedTransactions;

    public static AccountDto fromAccount(Account account) {
        return AccountDto.builder()
                .id(account.getId())
                .amount(account.getAmount())
                .accountRef(account.getAccountRef())
                .currency(account.getCurrency())
                .name(account.getName())
                .receivedTransactions(account.getReceivedTransactions().stream().map(TransactionDto::fromTransactio).collect(Collectors.toList()))
                .sentTransactions(account.getSentTransactions().stream().map(TransactionDto::fromTransactio).collect(Collectors.toList()))
                .build();
    }
}
