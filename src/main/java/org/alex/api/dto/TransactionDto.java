package org.alex.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Value;
import org.alex.api.entity.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@Value
public class TransactionDto {
    private int id;
    private String sourceAccount;
    private String targetAccount;
    private BigDecimal amount;
    private String currency;
    private Transaction.Status status;
    private LocalDateTime transactionTime;
    private String description;

    public static TransactionDto fromTransactio(Transaction transaction) {
        return TransactionDto.builder()
                .id(transaction.getId())
                .amount(transaction.getAmount())
                .sourceAccount(transaction.getSourceAccount().getAccountRef())
                .targetAccount(transaction.getTargetAccount().getAccountRef())
                .status(transaction.getStatus())
                .transactionTime(transaction.getTransactionTime())
                .description(transaction.getDescription())
                .currency(transaction.getCurrency())
                .build();
    }
}
