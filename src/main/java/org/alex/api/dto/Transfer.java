package org.alex.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transfer {
    @JsonProperty(required = true)
    private String sourceAccount;
    @JsonProperty(required = true)
    private String targetAccount;
    @JsonProperty(required = true)
    private BigDecimal amount;
    @JsonProperty(required = true)
    private String currency;
    private Optional<String> reference;

    @JsonCreator
    public Transfer(@JsonProperty(value = "sourceAccount", required = true) String sourceAccount,
                    @JsonProperty(value = "targetAccount", required = true) String targetAccount,
                    @JsonProperty(value = "amount", required = true) BigDecimal amount,
                    @JsonProperty(value = "currency", required = true) String currency) {
        this.sourceAccount = sourceAccount;
        this.targetAccount = targetAccount;
        this.amount = amount;
        this.currency = currency;
    }
}
