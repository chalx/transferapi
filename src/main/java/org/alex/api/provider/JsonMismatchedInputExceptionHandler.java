package org.alex.api.provider;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@Slf4j
public class JsonMismatchedInputExceptionHandler implements ExceptionMapper<MismatchedInputException> {

    @Override
    public Response toResponse(MismatchedInputException exception) {
        log.warn(exception.getMessage(), exception);

        String message = exception.getOriginalMessage();
        String requiredFieldMessage = message.substring(message.indexOf('\'') + 1, message.lastIndexOf('\''));

        return Response.status(400)
                .entity(Collections.singletonMap("message", "Field " + requiredFieldMessage + " is required"))
                .build();
    }
}
