package org.alex.api.provider;

import com.fasterxml.jackson.core.JsonParseException;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@Slf4j
public class JsonParseExceptionHandler implements ExceptionMapper<JsonParseException> {
    @Override
    public Response toResponse(JsonParseException exception) {
        log.warn(exception.getMessage(), exception);
        return Response.status(400).entity(Collections.singletonMap("message", "Malformed data")).build();
    }
}
