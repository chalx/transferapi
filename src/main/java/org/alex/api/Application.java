package org.alex.api;

import org.alex.api.provider.JsonMismatchedInputExceptionHandler;
import org.glassfish.jersey.server.ResourceConfig;

public class Application extends ResourceConfig {
    public Application() {
        packages("org.alex.api.provider");

        RootComponent rootComponent = DaggerRootComponent.create();

        register(JsonMismatchedInputExceptionHandler.class);
        register(rootComponent.getApiRoot());
    }
}
