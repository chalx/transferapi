package org.alex.api.dao;

import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.alex.api.exception.FailedException;
import org.alex.api.exception.InternalException;
import org.alex.api.exception.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.Map;

@Slf4j
abstract class AbstractDao<T> {

    final SessionFactory sessionFactory;

    AbstractDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    abstract Either<FailedException, T> findById(long id);

    public Try<Void> save(T entity) {
        return Try.run(() -> {
            Transaction transaction = null;
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();
                session.persist(entity);
                session.flush();
                transaction.commit();
            } catch (Exception ex) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw ex;
            }
        })
        .onFailure(ex -> log.error(ex.getMessage(), ex));
    }

    Either<FailedException, T> tryToRetrieveData(String query, Map<String, Object> params) {
        return Try.of(() -> {
                try (Session session = sessionFactory.openSession()) {
                    Query q = session.createQuery(query);
                    params.forEach(q::setParameter);
                    return (T) q.getSingleResult();
                }
            })
            .recoverWith(ex -> {
                log.warn("Account retrieve went wrong", ex);
                if (ex instanceof NoResultException) {
                    return Try.failure(new NotFoundException("Account does not exists", ex));
                }
                return Try.failure(new InternalException("Something went wrong", ex));
            })
            .toEither()
            .mapLeft(ex -> (FailedException) ex);
    }
}
