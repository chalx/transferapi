package org.alex.api.dao;

import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.alex.api.entity.Account;
import org.alex.api.entity.Transaction;
import org.alex.api.exception.FailedException;
import org.alex.api.exception.InternalException;
import org.alex.api.exception.RetryTransferException;
import org.alex.api.service.transfer.TransferInformation;
import org.hibernate.LockMode;
import org.hibernate.PessimisticLockException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collections;

@Singleton
@Slf4j
public class TransactionDao extends AbstractDao<Transaction>  {

    @Inject
    public TransactionDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    Either<FailedException, Transaction> findById(long id) {
        return tryToRetrieveData("from Transaction where id = :id", Collections.singletonMap("id", id));
    }

    public Try<Void> saveTransferInformation(TransferInformation transferInformation) {
        return Try.run(() -> {
            org.hibernate.Transaction transaction = null;
            try(Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();

                Account sourceAccount = session.get(Account.class, transferInformation.getSourceAccount().getId(), LockMode.PESSIMISTIC_FORCE_INCREMENT);
                Account targetAccount = session.get(Account.class, transferInformation.getTargetAccount().getId(), LockMode.PESSIMISTIC_FORCE_INCREMENT);

                sourceAccount.setAmount(transferInformation.getSourceAccount().getAmount());
                targetAccount.setAmount(transferInformation.getTargetAccount().getAmount());

                session.saveOrUpdate(sourceAccount);
                session.saveOrUpdate(targetAccount);
                session.persist(Transaction.fromTransfer(transferInformation));

                session.flush();
                session.clear();
                transaction.commit();
            } catch (PessimisticLockException ex) {
                if (transaction != null) {
                    transaction.rollback();
                }

                throw new RetryTransferException(ex.getMessage(), ex);
            } catch  (Exception ex) {
                tryToCloseTransaction(transaction, ex);
            }
        })
        .onFailure(ex -> log.error("Something went wrong", ex));
    }

    private void tryToCloseTransaction(org.hibernate.Transaction transaction, Exception ex) {
        try {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new InternalException("Something went wrong", ex);

        } catch (Exception e) {
            throw new InternalException("Something went wrong", e);
        }
    }
}

