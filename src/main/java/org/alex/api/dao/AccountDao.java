package org.alex.api.dao;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.alex.api.exception.FailedException;
import org.alex.api.entity.Account;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collections;

@Singleton
@Slf4j
public class AccountDao extends AbstractDao<Account> {
    @Inject
    public AccountDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Either<FailedException, Account> findById(long id) {
        return tryToRetrieveData("from Account where id = :id", Collections.singletonMap("id", id));
    }

    public Either<FailedException, Account> findByAccountRef(String accountReference) {
        return tryToRetrieveData("from Account where accountRef = :accountRef", Collections.singletonMap("accountRef", accountReference));
    }
}
