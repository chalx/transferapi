package org.alex.api.service.transfer;

import io.vavr.collection.Seq;
import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.alex.api.dto.Transfer;
import org.alex.api.dto.TransferStatus;
import org.alex.api.entity.Account;
import org.alex.api.exception.FailedException;
import org.alex.api.exception.RetryTransferException;
import org.alex.api.service.AccountService;
import org.alex.api.service.TransactionService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
@Slf4j
public class TransferService {
    private final AccountService accountService;
    private final TransferValidation transferValidation;
    private final TransactionService transactionService;

    @Inject
    public TransferService(AccountService accountService,
                           TransferValidation transferValidation,
                           TransactionService transactionService) {
        this.accountService = accountService;
        this.transferValidation = transferValidation;
        this.transactionService = transactionService;
    }

    public Either<FailedException, TransferStatus> transfer(Transfer transfer) {
        Either<FailedException, TransferStatus> transferResult = tryTransfer(transfer);

        if (transferResult.isRight()) {
            return transferResult;
        }

        if (transferResult.isLeft() && transferResult.getLeft() instanceof RetryTransferException) {
            return transfer(transfer);
        }

        return transferResult;
    }

    private Either<FailedException, TransferStatus> tryTransfer(Transfer transfer) {
        return loadAccountsIntoTransferInformation(transfer)
                .flatMap(transferValidation::checkAccountsAreInTheSameCurrency)
                .flatMap(transferValidation::checkTransferAmountIsGreaterThanZero)
                .flatMap(transferValidation::checkSourceAccountHasEnoughMoney)
                .map(this::updateAccountsAmount)
                .map(setReferenceMessage(transfer))
                .flatMap(transactionService::doTransfer);
    }

    private Function<TransferInformation, TransferInformation> setReferenceMessage(Transfer transfer) {
        return transferInformation -> {
            transferInformation.setMessage(transfer.getReference().orElse(""));
            return transferInformation;
        };
    }

    private TransferInformation updateAccountsAmount(TransferInformation transferInformation) {
        Account sourceAccount = transferInformation.getSourceAccount();
        Account targetAccount = transferInformation.getTargetAccount();

        sourceAccount.setAmount(sourceAccount.getAmount().subtract(transferInformation.getTransfer().getAmount()));
        targetAccount.setAmount(targetAccount.getAmount().add(transferInformation.getTransfer().getAmount()));

        return transferInformation;
    }

    private Either<FailedException, TransferInformation> loadAccountsIntoTransferInformation(Transfer transfer) {
        return Either.sequence(Arrays.asList(accountService.getAccountByReference(transfer.getSourceAccount()), accountService.getAccountByReference(transfer.getTargetAccount())))
              .peekLeft(failed ->log.warn(failed.map(FailedException::getMessage).collect(Collectors.joining("; "))))
              .bimap(failed -> failed.get(0), produceTransactionInformation(transfer));
    }

    private Function<Seq<Account>, TransferInformation> produceTransactionInformation(Transfer transfer) {
        return accounts -> TransferInformation.builder()
                .sourceAccount(accounts.get(0))
                .targetAccount(accounts.get(1))
                .transfer(transfer)
                .build();
    }
}
