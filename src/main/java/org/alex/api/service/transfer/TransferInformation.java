package org.alex.api.service.transfer;

import lombok.Builder;
import lombok.Data;
import org.alex.api.dto.Transfer;
import org.alex.api.entity.Account;
import org.alex.api.entity.Transaction;

@Data
@Builder
public class TransferInformation {
    private Account sourceAccount;
    private Account targetAccount;
    private Transfer transfer;
    private Transaction.Status status;
    private String message;

    public Transaction.Status getStatus() {
        if (status == null) {
            return Transaction.Status.COMPLETED;
        }

        return status;
    }
}
