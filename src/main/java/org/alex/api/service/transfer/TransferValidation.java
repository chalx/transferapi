package org.alex.api.service.transfer;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.alex.api.entity.Transaction;
import org.alex.api.exception.FailedException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;

@Singleton
@Slf4j
public class TransferValidation {

    @Inject
    public TransferValidation() {
        //needed by dagger
    }

    Either<FailedException, TransferInformation> checkAccountsAreInTheSameCurrency(TransferInformation transferInformation) {
        if (!transferInformation.getSourceAccount().getCurrency().equals(transferInformation.getTransfer().getCurrency())) {
            transferInformation.setStatus(Transaction.Status.FAILED);
            transferInformation.setMessage("Account has different currency than transfer");
            log.info("Account has different currency");
            return Either.left(new FailedException(400, "Account has different currency"));
        }

        if (!transferInformation.getSourceAccount().getCurrency().equals(transferInformation.getTargetAccount().getCurrency())) {
            transferInformation.setStatus(Transaction.Status.FAILED);
            transferInformation.setMessage("Different currency");
            log.info("Account currency should be the same.");
            return Either.left(new FailedException(400, "Account currency should be the same."));
        }

        return Either.right(transferInformation);
    }

    Either<FailedException, TransferInformation> checkTransferAmountIsGreaterThanZero(TransferInformation transferInformation) {
        if (transferInformation.getTransfer().getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            transferInformation.setStatus(Transaction.Status.FAILED);
            transferInformation.setMessage("Transfer amount is lower than 0");
            log.warn("Amount should be greater than 0");
            return Either.left(new FailedException(400, "Amount should be greater than 0"));
        }

        return Either.right(transferInformation);
    }

    Either<FailedException, TransferInformation> checkSourceAccountHasEnoughMoney(TransferInformation transferInformation) {
        if (transferInformation.getSourceAccount().getAmount().compareTo(transferInformation.getTransfer().getAmount()) < 0) {
            transferInformation.setMessage("Not enough money into account.");
            transferInformation.setStatus(Transaction.Status.FAILED);
            log.info("Not enough money for transfer");
            return Either.left(new FailedException(400, "Not enough money for transfer"));
        }

        return Either.right(transferInformation);
    }
}
