package org.alex.api.service;

import io.vavr.control.Either;
import org.alex.api.dao.TransactionDao;
import org.alex.api.dto.TransferStatus;
import org.alex.api.entity.Transaction;
import org.alex.api.exception.FailedException;
import org.alex.api.service.transfer.TransferInformation;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TransactionService {

    private final TransactionDao transactionDao;

    @Inject
    public TransactionService(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    public Either<FailedException, TransferStatus> doTransfer(TransferInformation transferInformation) {
         return transactionDao.saveTransferInformation(transferInformation)
                 .toEither()
                 .map(this::transferCompletedStatus)
                 .mapLeft(ex -> (FailedException) ex);
    }

    private TransferStatus transferCompletedStatus(Void exclude) {
        return TransferStatus.builder().message("Transfer completed").status(Transaction.Status.COMPLETED).build();
    }
}
