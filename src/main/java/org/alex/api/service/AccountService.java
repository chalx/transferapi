package org.alex.api.service;

import io.vavr.control.Either;
import io.vavr.control.Try;
import org.alex.api.dao.AccountDao;
import org.alex.api.exception.FailedException;
import org.alex.api.entity.Account;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AccountService {
    private final AccountDao accountDao;

    @Inject
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public Try<Void> save(Account account) {
        return accountDao.save(account);
    }

    public Either<FailedException, Account> getAccountByReference(String accountReference) {
        return accountDao.findByAccountRef(accountReference);
    }
}
