package org.alex.api.apis;

import org.alex.api.dto.AccountDto;
import org.alex.api.entity.Account;
import org.alex.api.service.AccountService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class MockApi {

    private final AccountService accountService;

    @Inject
    public MockApi(AccountService accountService) {
        this.accountService = accountService;
    }

    @POST
    public void mock() {
        accountService.save(Account.builder()
            .accountRef("sourceAccount")
            .currency("EUR")
            .amount(BigDecimal.valueOf(124.45))
            .name("Account test 1")
            .build());

        accountService.save(Account.builder()
                .accountRef("targetAccount")
                .currency("EUR")
                .amount(BigDecimal.ZERO)
                .name("Account test 2")
                .build());
    }

    @GET
    public Response getTargetAccount(@QueryParam("account") String accountRef) {
        return accountService.getAccountByReference(accountRef)
                .map(account -> Response.ok(AccountDto.fromAccount(account)).build())
                .mapLeft(f -> Response.status(500).entity(f).build())
                .get();
    }
}
