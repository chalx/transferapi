package org.alex.api.apis;

import lombok.extern.slf4j.Slf4j;
import org.alex.api.dto.Transfer;
import org.alex.api.service.transfer.TransferService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.function.Function;

@Singleton
@Slf4j
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransferApi {
    private final TransferService transferService;

    @Inject
    public TransferApi(TransferService transferService) {
        this.transferService = transferService;
    }

    @POST
    public Response transfer(Transfer transferDto) {
        return transferService.transfer(transferDto)
                .map(data -> Response.ok(data).build())
                .fold(ex -> Response.status(ex.getStatusCode()).entity(Collections.singletonMap("message", ex.getMessage())).build(), Function.identity());
    }
}
