package org.alex.api.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "account")
@SelectBeforeUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, unique = true)
    private String accountRef;
    @Column(nullable = false)
    private BigDecimal amount;
    @Column(nullable = false)
    private String currency;
    @OneToMany(targetEntity = Transaction.class, mappedBy = "sourceAccount", fetch = FetchType.EAGER)
    private List<Transaction> sentTransactions;
    @OneToMany(targetEntity = Transaction.class, mappedBy = "targetAccount", fetch = FetchType.EAGER)
    private List<Transaction> receivedTransactions;
    @Version
    @Column
    private int version;
}
