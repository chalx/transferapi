package org.alex.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.alex.api.dto.Transfer;
import org.alex.api.service.transfer.TransferInformation;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(targetEntity = Account.class)
    @JsonIgnore
    private Account sourceAccount;
    @ManyToOne(targetEntity = Account.class)
    @JsonIgnore
    private Account targetAccount;
    @Column(nullable = false)
    private BigDecimal amount;
    @Column(nullable = false)
    private String currency;
    @Column(nullable = false)
    private Status status;
    @Column(nullable = false)
    private LocalDateTime transactionTime;
    @Column
    private String description;


    public enum Status {
        COMPLETED, FAILED
    }

    public static Transaction fromTransfer(TransferInformation transferInformation) {
        Transfer transfer = transferInformation.getTransfer();
        return Transaction.builder()
                .sourceAccount(transferInformation.getSourceAccount())
                .targetAccount(transferInformation.getTargetAccount())
                .amount(transfer.getAmount())
                .currency(transfer.getCurrency())
                .transactionTime(LocalDateTime.now())
                .status(transferInformation.getStatus())
                .description(transferInformation.getMessage())
                .build();
    }
}
