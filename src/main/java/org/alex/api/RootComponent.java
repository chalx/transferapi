package org.alex.api;

import dagger.Component;

import javax.inject.Singleton;

@Component(modules = {RootModule.class})
@Singleton
public interface RootComponent {
    ApiRoot getApiRoot();
}
