package org.alex.api.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class InternalException extends FailedException {
    public InternalException(String message, Throwable cause) {
        super(500, message, cause);
    }

    public InternalException(String message) {
        super(500, message, null);
    }
}
