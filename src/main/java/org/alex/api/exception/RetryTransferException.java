package org.alex.api.exception;

public class RetryTransferException extends FailedException {
    public RetryTransferException(String message, Throwable cause) {
        super(500, message, cause);
    }
}
