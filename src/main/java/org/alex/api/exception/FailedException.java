package org.alex.api.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FailedException extends RuntimeException {
    private final int statusCode;

    public FailedException(int statusCode, String message, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public FailedException(int statusCode, String message) {
        super(message);

        this.statusCode = statusCode;
    }
}
