package org.alex.api.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NotFoundException extends FailedException {
    public NotFoundException(String message, Throwable cause) {
        super(404, message, cause);
    }
}

