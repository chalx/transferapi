package org.alex.api;

import dagger.Module;
import dagger.Provides;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.inject.Singleton;


@Module
public class RootModule {

    private RootModule() {}

    @Provides
    @Singleton
    public static SessionFactory providesSessionFactory() {
        return new Configuration().configure().buildSessionFactory();
    }
}
