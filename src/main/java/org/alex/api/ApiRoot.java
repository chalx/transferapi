package org.alex.api;

import org.alex.api.apis.MockApi;
import org.alex.api.apis.TransferApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Path;

@Path("api")
@Singleton
public class ApiRoot {
    private final MockApi mockApi;
    private final TransferApi transferApi;

    @Inject
    public ApiRoot(TransferApi transferApi,
                   MockApi mockApi) {
        this.transferApi = transferApi;
        this.mockApi = mockApi;
    }

    @Path("transfer")
    public TransferApi getTransferApi() {
        return transferApi;
    }

    @Path("mock")
    public MockApi getMockApi() {
        return mockApi;
    }
}
