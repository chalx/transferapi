package org.alex.api;

import io.vavr.control.Try;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Start {

    private static final Logger LOG = Logger.getLogger(Start.class.getCanonicalName());

    public static void main(String ... args) throws Exception {
        Server server = new Server(getPort());

        server.setHandler(createNewHandler());

        server.start();
        server.join();
    }

    private static int getPort() {
        return Try.of(() -> Integer.parseInt(System.getenv("PORT")))
                .peek(port -> LOG.info("Server started at port: " + port))
                .onFailure(ex -> LOG.log(Level.SEVERE, "Port is not correctly formatted. Fallback to 8080"))
                .getOrElse(8080);
    }

    private static Handler createNewHandler() {
        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        servletContextHandler.setContextPath("/");

        ServletHolder servletHolder  = servletContextHandler.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        servletHolder.setInitOrder(0);
        servletHolder.setInitParameter("javax.ws.rs.Application", Application.class.getCanonicalName());

        return servletContextHandler;
    }
}
