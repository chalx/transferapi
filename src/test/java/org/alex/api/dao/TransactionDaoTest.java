package org.alex.api.dao;

import io.vavr.control.Either;
import org.alex.api.exception.FailedException;
import org.alex.api.exception.InternalException;
import org.alex.api.exception.NotFoundException;
import org.alex.api.entity.Account;
import org.alex.api.entity.Transaction;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Test;

import javax.persistence.NoResultException;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionDaoTest {
    private SessionFactory sessionFactory = mock(SessionFactory.class);
    private TransactionDao transactionDao = new TransactionDao(sessionFactory);
    private Session session = mock(Session.class);
    private Query query = mock(Query.class);

    @Test
    public void ifSessionFailsToCreateReturnInternalException() {
        when(sessionFactory.openSession()).thenThrow(HibernateException.class);

        Either<FailedException, Transaction> result = transactionDao.findById(1);

        assertThat(result.getLeft()).isInstanceOf(InternalException.class);
        assertThat(result.getLeft().getStatusCode()).isEqualTo(500);
    }

    @Test
    public void ifAccountNotFoundByIdReturnNotFoundException() {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.getSingleResult()).thenThrow(NoResultException.class);

        Either<FailedException, Transaction> result = transactionDao.findById(1);

        assertThat(result.getLeft()).isInstanceOf(NotFoundException.class);
        assertThat(result.getLeft().getStatusCode()).isEqualTo(404);
    }

    @Test
    public void ifAccountFoundByIdReturnData() {
        Account sourceAccount = Account.builder()
                .id(1)
                .accountRef("AccountRef")
                .currency("EUR")
                .name("Alex")
                .build();

        Account targetAccount = Account.builder()
                .id(2)
                .accountRef("AccountRef")
                .currency("EUR")
                .name("Alex Account")
                .build();

        Transaction transaction = Transaction.builder()
                .id(1)
                .sourceAccount(sourceAccount)
                .targetAccount(targetAccount)
                .amount(BigDecimal.ONE)
                .currency("EUR")
                .transactionTime(LocalDateTime.now())
                .build();

        when(sessionFactory.openSession()).thenReturn(session);
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.getSingleResult()).thenReturn(transaction);

        Either<FailedException, Transaction> result = transactionDao.findById(1);

        assertThat(result.get()).isEqualTo(transaction);
    }
}
