package org.alex.api.dao;

import io.vavr.control.Either;
import org.alex.api.exception.FailedException;
import org.alex.api.exception.InternalException;
import org.alex.api.exception.NotFoundException;
import org.alex.api.entity.Account;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Test;

import javax.persistence.NoResultException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountDaoTest {

    private SessionFactory sessionFactory = mock(SessionFactory.class);
    private AccountDao accountDao = new AccountDao(sessionFactory);
    private Session session = mock(Session.class);
    private Query query = mock(Query.class);

    @Test
    public void ifSessionFailsToCreateReturnInternalException() {
        when(sessionFactory.openSession()).thenThrow(HibernateException.class);

        Either<FailedException, Account> result = accountDao.findById(1);

        assertThat(result.getLeft()).isInstanceOf(InternalException.class);
        assertThat(result.getLeft().getStatusCode()).isEqualTo(500);
    }

    @Test
    public void ifAccountNotFoundByIdReturnNotFoundException() {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.getSingleResult()).thenThrow(NoResultException.class);

        Either<FailedException, Account> result = accountDao.findById(1);

        assertThat(result.getLeft()).isInstanceOf(NotFoundException.class);
        assertThat(result.getLeft().getStatusCode()).isEqualTo(404);
    }

    @Test
    public void ifAccountFoundByIdReturnData() {
        Account account = Account.builder()
                .id(1)
                .accountRef("AccountRef")
                .currency("EUR")
                .name("Alex")
                .build();

        when(sessionFactory.openSession()).thenReturn(session);
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.getSingleResult()).thenReturn(account);

        Either<FailedException, Account> result = accountDao.findById(1);

        assertThat(result.get()).isEqualTo(account);
    }

    @Test
    public void ifAccountNotFoundByReferenceReturnNotFoundException() {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.getSingleResult()).thenThrow(NoResultException.class);

        Either<FailedException, Account> result = accountDao.findByAccountRef("accountRef");

        assertThat(result.getLeft()).isInstanceOf(NotFoundException.class);
        assertThat(result.getLeft().getStatusCode()).isEqualTo(404);
    }

    @Test
    public void ifAccountFoundByReferenceReturnData() {
        Account account = Account.builder()
                .id(1)
                .accountRef("AccountRef")
                .currency("EUR")
                .name("Alex")
                .build();

        when(sessionFactory.openSession()).thenReturn(session);
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.getSingleResult()).thenReturn(account);

        Either<FailedException, Account> result = accountDao.findByAccountRef("AccountRef");

        assertThat(result.get()).isEqualTo(account);
    }
}
