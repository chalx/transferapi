package org.alex.api.dao;

import io.vavr.control.Either;
import io.vavr.control.Try;
import org.alex.api.exception.FailedException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;

import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class AbstractDaoTest {
    private SessionFactory sessionFactory = mock(SessionFactory.class);
    private AbstractDao<Object> abstractDao = new AbstractDao<Object>(sessionFactory) {
        @Override
        Either<FailedException, Object> findById(long id) {
            return null; // this is tested in every dao
        }
    };
    private Session session = mock(Session.class);
    private Transaction transaction = mock(Transaction.class);

    @Test
    public void ifOpenSessionFailThenReturnFailure() {
        when(sessionFactory.openSession()).thenThrow(HibernateException.class);

        Try<Void> test = abstractDao.save("test");

        assertThat(test).isInstanceOf(Try.Failure.class);
        verify(session, times(0)).close();
    }

    @Test
    public void ifTransactionCommitFailThenReturnFailure() {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);
        doThrow(RollbackException.class).when(transaction).commit();

        Try<Void> test = abstractDao.save("test");

        assertThat(test).isInstanceOf(Try.Failure.class);
        verify(session, times(1)).close();
        verify(transaction, times(1)).commit();
        verify(transaction, times(1)).rollback();
    }

    @Test
    public void ifTransactionRollbackFailThenReturnFailure() {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);
        doThrow(RollbackException.class).when(transaction).commit();
        doThrow(PersistenceException.class).when(transaction).rollback();

        Try<Void> test = abstractDao.save("test");

        assertThat(test).isInstanceOf(Try.Failure.class);
        verify(session, times(1)).close();
        verify(transaction, times(1)).commit();
        verify(transaction, times(1)).rollback();
    }

    @Test
    public void ifEntityWasSavedReturnSuccess() {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);
        doNothing().when(transaction).commit();

        Try<Void> test = abstractDao.save("test");

        assertThat(test).isInstanceOf(Try.Success.class);
        verify(session, times(1)).close();
        verify(transaction, times(1)).commit();
        verify(transaction, times(0)).rollback();
    }

}
