package org.alex.api.service.transfer;

import io.vavr.control.Either;
import org.alex.api.DaggerRootComponent;
import org.alex.api.RootComponent;
import org.alex.api.dao.AccountDao;
import org.alex.api.dao.TransactionDao;
import org.alex.api.dto.AccountDto;
import org.alex.api.dto.Transfer;
import org.alex.api.dto.TransferStatus;
import org.alex.api.entity.Account;
import org.alex.api.entity.Transaction;
import org.alex.api.exception.FailedException;
import org.alex.api.exception.InternalException;
import org.alex.api.exception.NotFoundException;
import org.alex.api.service.AccountService;
import org.alex.api.service.TransactionService;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class TransferServiceTest {

    private Query query = mock(Query.class);
    private Session sessionAccount = mock(Session.class);
    private Session sessionTransaction = mock(Session.class);
    private SessionFactory sessionFactoryAccount = mock(SessionFactory.class);
    private SessionFactory sessionFactoryTransaction = mock(SessionFactory.class);
    private AccountDao accountDao = new AccountDao(sessionFactoryAccount);
    private AccountService accountService = new AccountService(accountDao);
    private TransactionDao transactionDao = new TransactionDao(sessionFactoryTransaction);
    private TransactionService transactionService = new TransactionService(transactionDao);
    private TransferService transferService = new TransferService(accountService, new TransferValidation(), transactionService);

    @Before
    public void setUp() {

        Mockito.reset(
                query,
                sessionAccount,
                sessionTransaction,
                sessionFactoryAccount,
                sessionFactoryTransaction
        );
    }

    @Test
    public void ifSourceAccountNotExistsReturnTransferStatusFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(BigDecimal.ZERO)
                .build();

        when(accountDao.findByAccountRef("testAccountSource")).thenThrow(NoResultException.class);

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(NotFoundException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Account does not exists");

        verify(sessionTransaction, times(0)).update(any(Transaction.class));
        verify(sessionTransaction, times(0)).save(any(Account.class));
    }

    @Test
    public void ifTargetAccountNotExistsReturnTransferStatusFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(BigDecimal.ZERO)
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult()).thenReturn(Account.builder().build());

        when(accountDao.findByAccountRef("testAccountTarget")).thenThrow(NoResultException.class);

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(NotFoundException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Account does not exists");

        verify(sessionTransaction, times(0)).update(any(Transaction.class));
        verify(sessionTransaction, times(0)).save(any(Account.class));
    }

    @Test
    public void sourceAccountHasDifferentCurrencyThanTransferThenReturnStatusFailed() {
        Transfer transfer = Transfer.builder()
                .currency("RON")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(BigDecimal.ZERO)
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().currency("EUR").build())
                .thenReturn(Account.builder().currency("EUR").build());

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(FailedException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Account has different currency");

        verify(sessionTransaction, times(0)).update(any(Account.class));
        verify(sessionTransaction, times(0)).save(any(Transaction.class));
    }

    @Test
    public void accountAreOnDifferentCurrencyThenReturnStatusFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(BigDecimal.ZERO)
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().currency("EUR").build())
                .thenReturn(Account.builder().currency("RON").build());

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(FailedException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Account currency should be the same.");

        verify(sessionTransaction, times(0)).update(any(Account.class));
        verify(sessionTransaction, times(0)).save(any(Transaction.class));
    }

    @Test
    public void tryToTransferZeroAmountThenReturnStatusFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(BigDecimal.ZERO)
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().currency("EUR").build())
                .thenReturn(Account.builder().currency("EUR").build());

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(FailedException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Amount should be greater than 0");

        verify(sessionTransaction, times(0)).update(any(Account.class));
        verify(sessionTransaction, times(0)).save(any(Transaction.class));
    }

    @Test
    public void tryToTransferNegativeAmountThenReturnStatusFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(new BigDecimal(-1))
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().currency("EUR").build())
                .thenReturn(Account.builder().currency("EUR").build());

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(FailedException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Amount should be greater than 0");

        verify(sessionTransaction, times(0)).update(any(Account.class));
        verify(sessionTransaction, times(0)).save(any(Transaction.class));
    }

    @Test
    public void ifSourceAccountHasLessAmountThenTransferFail() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(new BigDecimal(90))
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().amount(new BigDecimal(1)).currency("EUR").build())
                .thenReturn(Account.builder().currency("EUR").build());

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);
        assertThat(result.getLeft()).isInstanceOf(FailedException.class);
        assertThat(result.getLeft().getMessage()).isEqualTo("Not enough money for transfer");

        verify(sessionTransaction, times(0)).update(any(Account.class));
        verify(sessionTransaction, times(0)).save(any(Transaction.class));
    }

    @Test
    public void ifPesimisticLockFailRetry() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(new BigDecimal(90))
                .reference(Optional.empty())
                .build();
        Account sourceAccount = Account.builder().id(1).amount(new BigDecimal(190)).currency("EUR").build();
        Account targetAccount = Account.builder().id(2).amount(new BigDecimal(0)).currency("EUR").build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenReturn(sessionTransaction);

        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().amount(new BigDecimal(190)).currency("EUR").build());

        org.hibernate.Transaction tr = mock(org.hibernate.Transaction.class);
        when(sessionTransaction.beginTransaction()).thenReturn(tr);
        doThrow(PessimisticLockException.class)
                .doNothing()
                .when(tr)
                .commit();

        when(sessionTransaction.get(eq(Account.class), any(Integer.class), eq(LockMode.PESSIMISTIC_FORCE_INCREMENT)))
                .thenReturn(sourceAccount)
                .thenReturn(targetAccount);

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);

        assertThat(result.get()).isInstanceOf(TransferStatus.class);
        assertThat(result.get().getStatus()).isEqualTo(Transaction.Status.COMPLETED);

        verify(tr, times(1)).rollback();
        verify(tr, times(2)).commit();
        verify(sessionTransaction, times(4)).saveOrUpdate(any(Account.class));
        verify(sessionTransaction, times(2)).persist(any(Transaction.class));
    }

    @Test
    public void ifInternalErrorOccurredOnOptainingSessionSendFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(new BigDecimal(90))
                .reference(Optional.empty())
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenReturn(sessionTransaction);

        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().amount(new BigDecimal(190)).currency("EUR").build());

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenThrow(HibernateException.class);

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);

        assertThat(result.getLeft()).isInstanceOf(InternalException.class);
    }

    @Test
    public void ifInternalErrorOccurredOnCommitSendFailed() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(new BigDecimal(90))
                .reference(Optional.empty())
                .build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenReturn(sessionTransaction);

        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(Account.builder().amount(new BigDecimal(190)).currency("EUR").build());

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenReturn(sessionTransaction);
        org.hibernate.Transaction transactionSessionTransaction = mock(org.hibernate.Transaction.class);
        when(sessionTransaction.beginTransaction()).thenReturn(transactionSessionTransaction);
        doThrow(RollbackException.class)
                .when(transactionSessionTransaction)
                .commit();

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);

        assertThat(result.getLeft()).isInstanceOf(InternalException.class);
    }

    @Test
    public void transferCompletedWithSuccess() {
        Transfer transfer = Transfer.builder()
                .currency("EUR")
                .sourceAccount("testAccountSource")
                .targetAccount("testAccountTarget")
                .amount(new BigDecimal(190))
                .reference(Optional.empty())
                .build();

        Account sourceAccount = Account.builder().id(1).amount(new BigDecimal(190)).currency("EUR").build();
        Account targetAccount = Account.builder().id(2).amount(new BigDecimal(0)).currency("EUR").build();

        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenReturn(sessionTransaction);

        when(sessionAccount.createQuery(any(String.class))).thenReturn(query);
        when(query.getSingleResult())
                .thenReturn(sourceAccount)
                .thenReturn(targetAccount);

        when(sessionTransaction.get(eq(Account.class), any(Integer.class), eq(LockMode.PESSIMISTIC_FORCE_INCREMENT)))
                .thenReturn(sourceAccount)
                .thenReturn(targetAccount);

        org.hibernate.Transaction transaction = mock(org.hibernate.Transaction.class);
        when(sessionFactoryAccount.openSession()).thenReturn(sessionAccount);
        when(sessionFactoryTransaction.openSession()).thenReturn(sessionTransaction);
        when(sessionTransaction.beginTransaction()).thenReturn(transaction);

        Either<FailedException, TransferStatus> result = transferService.transfer(transfer);

        assertThat(result.get()).isInstanceOf(TransferStatus.class);
        assertThat(result.get().getStatus()).isEqualTo(Transaction.Status.COMPLETED);

        assertThat(sourceAccount.getAmount()).isEqualTo(BigDecimal.ZERO);
        assertThat(targetAccount.getAmount()).isEqualTo(new BigDecimal(190));

        verify(sessionTransaction, times(2)).saveOrUpdate(any(Account.class));
        verify(sessionTransaction, times(1)).persist(any(Transaction.class));
        verify(transaction, times(1)).commit();
        verify(transaction, times(0)).rollback();
    }

    @Test
    public void usingDatabase() {
        RootComponent app = DaggerRootComponent.create();

        app.getApiRoot().getMockApi().mock();
        Response response = app.getApiRoot().getTransferApi().transfer(Transfer.builder()
                .amount(new BigDecimal(10))
                .currency("EUR")
                .reference(Optional.of("Test"))
                .sourceAccount("sourceAccount")
                .targetAccount("targetAccount")
                .build());


        Response sourceAccount = app.getApiRoot().getMockApi().getTargetAccount("sourceAccount");
        Response targetAccount = app.getApiRoot().getMockApi().getTargetAccount("targetAccount");

        assertThat(((AccountDto)sourceAccount.getEntity()).getAmount()).isEqualByComparingTo(BigDecimal.valueOf(114.45));
        assertThat(((AccountDto)sourceAccount.getEntity()).getSentTransactions().size()).isEqualTo(1);
        assertThat(((AccountDto)sourceAccount.getEntity()).getReceivedTransactions().size()).isEqualTo(0);

        assertThat(((AccountDto)targetAccount.getEntity()).getAmount()).isEqualByComparingTo(BigDecimal.valueOf(10));
        assertThat(((AccountDto)targetAccount.getEntity()).getSentTransactions().size()).isEqualTo(0);
        assertThat(((AccountDto)targetAccount.getEntity()).getReceivedTransactions().size()).isEqualTo(1);

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.getEntity()).isEqualTo(TransferStatus.builder().message("Transfer completed").status(Transaction.Status.COMPLETED).build());
    }
}
