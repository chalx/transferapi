Transfer API
=====
![Build Status](https://img.shields.io/bitbucket/pipelines/chalx/transferapi.svg)

To run the project, clone it on your personal computer and then execute the following command:
```bash
./gradlew run
```

If you want to use other port than the default one(8080) then run the following command:
```bash
PORT=9091 ./gradlew run
```

The application exposes the following endpoints

| Endpoint | Description |
|----------|-------------|
| POST /api/mock | Creates two mock accounts |
| GET /api/mock?account=sourceAccount | Retrieve information about account |
| POST /api/transfer | Execute a transfer between two account |

To do a transfer you need to do a ``POST`` request to ``/api/transfer``` with the following body:
```json
{
	"sourceAccount": "sourceAccount",
	"targetAccount": "targetAccount",
	"amount": 20, 
	"currency": "EUR",
	"reference": "hellop"
}
```

* ``sourceAccount`` refers to ``accountRef`` not to id
* ``targetAccount`` refers to ``accountRef`` not to id

The response body will be:
```json
{
    "status": "COMPLETED",
    "message": "Transfer completed"
}
```

The mocked data contains the following accounts:
```json
{
    "id": 1,
    "name": "Account test 1",
    "accountRef": "sourceAccount",
    "amount": 104.45,
    "currency": "EUR"
}
```
```json
{
    "id": 2,
    "name": "Account test 2",
    "accountRef": "targetAccount",
    "amount": 104.45,
    "currency": "EUR"
}
```

# Data model
![Data model](https://www.lucidchart.com/publicSegments/view/feea60e7-ec7c-42c1-a0fb-2dfa7ca3c64a/image.png)